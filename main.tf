// Configure the Google Cloud provider
provider "google" {
 #credentials = file("terraform-338212-6a8757c53c45.json")
 project     = var.project_name
 region      = "us-west1"
}

// A single Compute Engine instance
resource "google_compute_instance" "default" {
 name         = "vm-node"
 machine_type = "f1-micro"
 zone         = "us-west1-a"
 #tags = "node"


#metadata_startup_script = "mkdir /$HOME/site; echo hellow > /$HOME/site.js; sudo mkdir /home/$USER/folder2; mkdir gggg"
 metadata = {
    startup-script = <<-EOF
                    cd
                    mkdir node_project
                    EOF
  } 
 /* metadata = {
    ssh-keys = "${var.USERNAME_SSH}:${file(GCP_SSH_KEY)}"
  } */
 boot_disk {
   initialize_params {
     image = "ubuntu-1804-bionic-v20211021"
   }
 }

 network_interface {
   network = "default"

   access_config {
     // Include this section to give the VM an external ip address
   }
 }
 }

/* resource "google_compute_network" "default" {
  name = "default"
}
 */
resource "google_compute_firewall" "default" {
 name    = "teste-firewall"
 network = "default"

 allow {
   protocol = "tcp"
   ports    = ["3000","80","8080"]
 }
 source_tags = ["default"]
}

resource "google_compute_project_metadata" "my_ssh_key" {
  metadata = {
    ssh-keys = <<EOF
      gabrielrosadias:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDNRLCIVHXCoB1tRUgW73zqMYirMaIvAFVlFWY7bS0OOkRskAICqYZl7S1iEa4+qkij1/hpmqTf3ifK6uhdOI/8nRWzgR6dFO0tmppslcKMppTBgkrzUgUatLCv2l3gDMzAcsE3MdgnErnCdgkIDgOimElzfSlzD5hNHq7KwgZe3NllJeRFzCzhK9sPJAYNRMo87/Mxv+WjwOVD6HY7McF8HqYDpE686B7fBz7sXYslwzIaeA3BQn+F906DnwfGmn3SAFN9Md9wGvgJTtzJ7wkDSTIvuydWm8JR+5hOMuV1mokKN0SzRSHv3IfvwN2ud92/zur/EBhem7TujP6y4hVkoFe0FXQCljvCJ9IopdiDIiWPZUcYTg6J4YqEN3h8fnx0a2pR+f+O6x3D0KxExmAp+YmnD7y8H/zISuxjrdHQ2BD1Axuw427JTVn8u79FOOD7JPw/zyJfQS4Sk90zp126KkiCSm9/iHkRRDJp2VeJpT671Wx9EKeRv9oxQmppq8E= gabrielrosadias@gmail.com
    EOF
  }
}
// A variable for extracting the external IP address of the instance


/* resource "google_storage_bucket" "tf-bucket" {
  #project       = var.gcp_project
  name          = "backend_bucket"
  location      = "US"
  force_destroy = true
  #storage_class = var.storage-class
  versioning {
    enabled = true
  }
} */

